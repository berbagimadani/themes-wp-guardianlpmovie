<?php
//session_start(); 
/**
 * Gen Themes functions and definitions.
 * @package WordPress
 * @subpackage Genthemes V1
 * @since genthemes v1
 * @web genthemes.net
 * @email genthemes@gmail.com
 */

  
if ( ! isset( $content_width ) )
	$content_width = 604;
	$root = get_template_directory_uri(); 
 
function lpmovieguardian_scripts_styles() {
	global $root;
}
add_action( 'wp_enqueue_scripts', 'lpmovieguardian_scripts_styles' );
 
function lpmovieguardian_setup() { 
	  
	load_theme_textdomain( '', get_template_directory() . '/languages' );
	add_editor_style();
	add_theme_support( 'automatic-feed-links' );
	add_theme_support( 'post-formats', array( 'aside', 'image', 'link', 'quote', 'status' ) );
	register_nav_menu( 'primary', __( 'Primary Menu', '' ) );
	register_nav_menu( 'one-page-menu', __( 'One Page Menu', '' ) );
	add_theme_support( 'custom-background', array(
		'default-color' => 'e6e6e6',
	) );
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 604, 270, true );
	if ( function_exists( 'add_image_size' ) ) { 
		add_image_size( 'sidebar-thumb', 150, 150, true ); // Hard Crop Mode
		add_image_size( 'category-thumb', 250); // Soft Crop Mode 
		add_image_size( 'relatedpost-thumb', 180, 160 );
	}
}
add_action( 'after_setup_theme', 'lpmovieguardian_setup' );
 
if ( ! function_exists( 'lpmovieguardian_comment' ) ) :
function lpmovieguardian_comment( $comment, $args, $depth ) {
	$GLOBALS['comment'] = $comment;
	switch ( $comment->comment_type ) :
		case 'pingback' :
		case 'trackback' :
		// Display trackbacks differently than normal comments.
	?>
	<li <?php comment_class(); ?> id="comment-<?php comment_ID(); ?>">
		<p><?php _e( 'Pingback:', '' ); ?> <?php comment_author_link(); ?> <?php edit_comment_link( __( '(Edit)', '' ), '<span class="edit-link">', '</span>' ); ?></p>
	<?php
			break;
		default :
		// Proceed with normal comments.
		global $post;
	?>
	 <!--{testing}-->

			<?php if ( '0' == $comment->comment_approved ) : ?>
				<p class="comment-awaiting-moderation"><?php _e( 'Your comment is awaiting moderation.', '' ); ?></p>
			<?php endif; ?>  
	<?php
		break;
	endswitch; // end comment_type check
}
endif;
function lpmovieguardian_enqueue_comments_reply() {
	if( get_option( 'thread_comments' ) )  {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'comment_form_before', 'lpmovieguardian_enqueue_comments_reply' );

// Setting body background css
function getOption($var){
	$options = get_option('wpviddycpa-option');
    if(!empty($var))
        return @$options[$var];
}
 
if ( ! function_exists( 'lpmovieguardian_content_nav' ) ) :
function lpmovieguardian_content_nav( $html_id ) {
	global $wp_query;   
	if ( $wp_query->max_num_pages > 1 ) : ?>
	<ul class="pagination">
		<?php 
				$big = 999999999; // need an unlikely integer 
				echo paginate_links( array(
					'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
					'format' => '?paged=%#%',
					'current' => max( 1, get_query_var('paged') ),
					'total' => $wp_query->max_num_pages
				) ); 
		?>
	</ul>
	<?php endif;
} 
endif;

/*  View post 
/* ------------------------------------ */	
if ( ! function_exists( 'wpb_set_post_views' ) ) {
	function wpb_set_post_views($postID) {
	    $count_key = 'wpb_post_views_count';
	    $count = get_post_meta($postID, $count_key, true);
	    if($count==''){
	        $count = 0;
	        delete_post_meta($postID, $count_key);
	        add_post_meta($postID, $count_key, '0');
	    }else{
	        $count++;
	        update_post_meta($postID, $count_key, $count);
	    }
	}
}
//To keep the count accurate, lets get rid of prefetching
remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);

if ( ! function_exists( 'view_count' ) ) {
	function view_count($postID){
	    $count_key = 'wpb_post_views_count';
	    $count = get_post_meta($postID, $count_key, true);
	    if($count==''){
	        delete_post_meta($postID, $count_key);
	        add_post_meta($postID, $count_key, '0');
	        return "0";
	    }
	    return $count;
	}
}
if ( ! function_exists( 'wpb_track_post_views' ) ) {
	function wpb_track_post_views ($post_id) {
	    if ( !is_single() ) return;
	    if ( empty ( $post_id) ) {
	        global $post;
	        $post_id = $post->ID;    
	    }
	    wpb_set_post_views($post_id);
	}
}
add_action( 'wp_head', 'wpb_track_post_views'); 

/*
 * 
 * CUSTOME PAGINATION
 */
function custom_pagination() {
    global $wp_query;
    $big = 999999999; // need an unlikely integer
    $pages = paginate_links( array(
            'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
            'format' => '?paged=%#%',
            'current' => max( 1, get_query_var('paged') ),
            'total' => $wp_query->max_num_pages,
            'prev_next' => false,
            'type'  => 'array',
            'prev_next'   => TRUE,
			'prev_text'    => __('<<'),
			'next_text'    => __('>>'),
        ) );
        if( is_array( $pages ) ) {
            $paged = ( get_query_var('paged') == 0 ) ? 1 : get_query_var('paged');
            echo '<ul class="pagination">';
            foreach ( $pages as $page ) {
                    echo "<li >$page</li>";
            }
           echo '</ul>';
        }
}
/*
 * 
 * CUSTOME BREADCRUMB
 */
function custom_breadcrumb() {
	if (!is_home()) { 
		if (is_category() || is_single()) {
			echo  '<li>'.the_category(' ').'</li>';
			echo '<li>'.get_the_title().'</li>';
		}	 
	}
}

function getSizeImage($attr) {
	$image_id = get_post_thumbnail_id();
	$image_url = wp_get_attachment_image_src($image_id,$attr, true);
	$link = '<a href="'.$image_url[0].'">'.$image_url[1]." x ".$image_url[2].'</a>';
	return $link;
}

function the_titlesmall($before = '', $after = '', $echo = true, $length = false) { $title = get_the_title();

	if ( $length && is_numeric($length) ) {
		$title = substr( $title, 0, $length );
	}

	if ( strlen($title)> 0 ) {
		$title = apply_filters('the_titlesmall', $before . $title . $after, $before, $after);
		if ( $echo )
			echo $title;
		else
			return $title;
	}
}

/*
*
*
*/
function introBackground() {
	$general_intro_video = getOption('general-intro-video');

	$ex_intro_video = explode(';',  $general_intro_video);
    $filter_intro_video = array_filter($ex_intro_video);  
    $intro_ran = array_rand($filter_intro_video);  
	return trim($filter_intro_video[$intro_ran]);
}
