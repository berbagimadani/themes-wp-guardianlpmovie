<?php
/**
 * Gen Themes Display.
 * @package WordPress
 * @subpackage Genthemes V1
 * @since genthemes v1
 * @web genthemes.net
 * @email genthemes@gmail.com
 */

get_header(); ?>
  
<?php while ( have_posts() ) : the_post(); ?>
	
	<?php get_template_part( 'content', get_post_format() ); ?> 

<?php endwhile; ?>
 
<?php get_footer(); ?>