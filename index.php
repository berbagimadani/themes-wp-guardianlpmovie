 
<?php get_header(); ?>
  
<?php if(is_home()){?> 

 <!-- Header 
    <a name="about"></a>
    <div class="intro-header">
        <div class="container">
            
            <div class="row main-background-" style="margin-top:20px;">
                <div class="col-lg-12 canvas-viddy-desc">
                    xx
                </div>
            </div>

        </div> 

    </div>
    intro-header-->

    <!-- Page Content -->

  <a  name="services"></a>
    <div class="content-section-a" style="padding-top:60px;">

         <div class="container">
            <div class="row no-getter"> 
                <div class="col-md-12"><h3 class="heading">Top Rated</h3></div>
                <?php 

                   //$currentCategory = single_cat_title("", false);
                    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                    $args = array(
                        'post_type' => SLUG_CUSTOME_POST,
                        //'gallery_category' => 'video-gallery',
                        'paged'=> $paged,
                        'order' => 'DESC',
                        'posts_per_page' => '6'
                    );  
                    $the_query = new WP_Query( $args ); 
                    if($the_query->have_posts()) :     

                    while ($the_query->have_posts()):
                        $the_query->the_post();   
                        $featured_image_array = wp_get_attachment_image_src( get_post_thumbnail_id(), 'single-post-thumbnail' );
                        $featured_image = $featured_image_array[0];
                   ?>  
                <div class="col-md-2 col-xs-4 box-thumb-images"> 
                    <div class="thumbnail no-border">
                        <a href="<?php echo get_permalink(); ?>"> 
                        <img class="img-responsive" src="<?php echo get_post_meta(get_the_ID(),'wpviddycpa-meta-image',TRUE) ?>">
                        <div class="caption-play rail__item-play theme__background-color-rgba-80">
                            <!--<img src="http://mtdb.info/assets/images/play.png">--><span></span>
                        </div>
                        <div class="caption">  
                            <span class="label-no-radius label-viddy-black" rel="tooltip" title="IMDB Rate">
                                <i class="fa fa-star-o yellow"></i> <?php echo get_post_meta(get_the_ID(),'wpviddycpa-meta-rating',TRUE) ?>
                            </span>  
                        </div>
                        </a>
                    </div>
                    <div class="box-title-thumb">
                        <a href="<?php echo get_permalink(); ?>" class="text-left">
                            <h5><?php echo get_the_title(); ?></h5> 
                        </a>
                    </div>
                </div>
                <?php endwhile; wp_reset_postdata();?>
                <?php else: ?>
                <div class="col-md-12">
                  <article>
                    <h2>Sorry...</h2>
                    <p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
                  </article>
                </div>
                <?php endif; ?>

                <div class="wpviddy-ads1">
                    <?php //echo getOption('ads-banner-home'); ?>
                </div>
                
            </div>

        </div>
        <!-- /.container -->

        <div class="container">
            <div class="row no-getter"> 
                <div class="col-md-12">
                    <h3 class="heading">Latest Movies</h3>  
                </div>
                <?php 

                   //$currentCategory = single_cat_title("", false);
                    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                    $args = array(
                        'post_type' => 'post',
                        //'gallery_category' => 'video-gallery',
                        'paged'=> $paged,
                        'order' => 'DESC',
                        'posts_per_page' => '12'
                    );  
                    $the_query = new WP_Query( $args ); 
                if($the_query->have_posts()) :     

                    while ($the_query->have_posts()):
                        $the_query->the_post();   
                        $featured_image_array = wp_get_attachment_image_src( get_post_thumbnail_id(), 'single-post-thumbnail' );
                        $featured_image = $featured_image_array[0];
                   ?>  
                <div class="col-md-2 col-xs-4 box-thumb-images"> 
                    <div class="thumbnail no-border">
                        <a href="<?php echo get_permalink(); ?>"> 
                        <img class="img-responsive" src="<?php echo get_post_meta(get_the_ID(),'wpviddycpa-meta-image',TRUE) ?>">
                        <div class="caption-play rail__item-play theme__background-color-rgba-80">
                            <span></span>
                        </div>
                        <div class="caption">  
                            <span class="label-no-radius label-viddy-black" rel="tooltip" title="IMDB Rate">
                                <i class="fa fa-star-o yellow"></i> <?php echo get_post_meta(get_the_ID(),'wpviddycpa-meta-rating',TRUE) ?>
                            </span>  
                        </div>
                        </a>
                    </div>
                    <div class="box-title-thumb">
                    <a href="<?php echo get_permalink(); ?>" class="text-left">
                        <h5><?php echo get_the_title(); ?></h5> 
                    </a>
                    </div>
                </div>
                <?php endwhile; wp_reset_postdata();?>
                
                <div class="col-md-12 pagination-guard">
                <?php if ($the_query->max_num_pages > 1) { // check if the max number of pages is greater than 1  ?>
                   <?php
                    $big = 999999999; // need an unlikely integer
                    echo paginate_links( array(
                        'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
                        'format' => '?paged=%#%',
                        'current' => max( 1, get_query_var('paged') ),
                        'total' => $the_query->max_num_pages
                    ) );
                   ?>
                <?php } ?>

                <?php else: ?>
                  <article>
                    <h2>Sorry...</h2>
                    <p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
                  </article>
                </div>
                <?php endif; ?>

                <div class="wpviddy-ads1">
                    <?php echo getOption('ads-banner-home'); ?>
                </div>
                
            </div>

        </div>
        <!-- /.container -->

    </div>
    <!-- /.content-section-a -->



<?php } ?>  
  
 
<?php get_footer(); ?>

<script type="text/javascript">
  var callback = $.cookie('callback');
  var callback_open = $.cookie('callback_open');
  console.log(callback +' == '+ callback_open); 
  <?php 
    $key_single = !empty($_GET['key']) ? $_GET['key'] : '';
    echo $_GET['key'] ;
    if( $key_single == 123 ) { 
  ?>
      $.cookie('callback_open', callback, { expires: 1, path: '/' } );
      window.location = callback;
  <?php } ?>
</script>
