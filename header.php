<?php 
/**
* Gen Themes Display.
* @package WordPress 
* @subpackage Genthemes V1
* @since genthemes v1
* @web genthemes.net
* @email genthemes@gmail.com
*/
?>
<!doctype html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="<?php echo getOption('favicon]');?>">

    <title>
    <?php if ( is_category() ) {
	echo 'Category Archive for &quot;'; single_cat_title(); echo '&quot; | '; bloginfo( 'name' );
	} elseif ( is_tag() ) {
		echo 'Tag Archive for &quot;'; single_tag_title(); echo '&quot; | '; bloginfo( 'name' );
	} elseif ( is_archive() ) {
		wp_title(''); echo ' Archive | '; bloginfo( 'name' );
	} elseif ( is_search() ) {
		echo 'Search for &quot;'._wp_specialchars($s).'&quot; | '; bloginfo( 'name' );
	} elseif ( is_home() || is_front_page() ) {
		bloginfo( 'name' ); echo ' | '; bloginfo( 'description' );
	}  elseif ( is_404() ) {
		echo 'Error 404 Not Found | '; bloginfo( 'name' );
	} elseif ( is_single() ) {
		wp_title('');
	} else {
		echo wp_title( ' | ', false, 'right' ); bloginfo( 'name' );
	} ?>
    </title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo get_template_directory_uri(); ?>/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template --> 
    <link href="<?php echo get_template_directory_uri().'/css/landing-page.css'; ?>" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri(); ?>/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    
    <link rel="stylesheet" href="https://cdn.plyr.io/1.6.16/plyr.css"> 
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(). '/social-share/jssocials.css'; ?>">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(). '/social-share/jssocials-theme-flat.css'; ?>">

    <link rel="stylesheet" href="http://callmenick.com/_development/five-star-rating/css/rating.min.css">


     
    <!-- jQuery -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
   
    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo  get_template_directory_uri(). '/js/bootstrap.min.js'; ?>"></script>
    <script src="http://callmenick.com/_development/five-star-rating/js/dist/rating.min.js"></script>

    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  <?php wp_head(); ?> 
  <?php $root = get_template_directory_uri(); ?>

  <?php echo getOption('ads-scripts'); ?>

  <div id="fb-root"></div>
  <script>(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.7&appId=430707847037600";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));</script>


 
    <style type="text/css">
      body{
        background: <?php echo getOption('background-body'); ?>
      }
      .content-section-a{
        background-color: <?php echo getOption('background-body'); ?>
      }
      .guard-top {
        background: <?php echo getOption('background-top-header'); ?>;
        border-bottom: 1px solid #383737;
      }
      .heading {
        background: <?php echo getOption('background-heading'); ?>;
        padding: 8px;
        color: #3E424D;
        display: block;
      }
      #menu-footer-menu li a {
        color: <?php echo getOption('font-color-footer'); ?>;
        font-size: 13px;
      }

      footer.bg-footer {
          background-color: <?php echo getOption('background-footer'); ?>;
          width: 100%;
          height: auto;
      }
      footer.bg-footer .copyright{
          color: <?php echo getOption('font-color-footer'); ?>;
      }

      /* menu nav bar */
      .nav.navbar-nav.yamm li a {
        color: <?php echo getOption('color-top-header'); ?>;
        text-transform: uppercase;
      }
      .nav.navbar-nav.yamm li a:hover {
        color: <?php echo getOption('color-top-header-hover'); ?>;
      } 
      .navbar-default .navbar-nav>.open>a, .navbar-default .navbar-nav>.open>a:focus, .navbar-default .navbar-nav>.open>a:hover {
        color: <?php echo getOption('color-top-header-hover'); ?>;
        background-color: <?php echo getOption('background-top-header'); ?>;
      }
      .dropdown-menu {
          background-color: <?php echo getOption('background-top-header'); ?>;
          margin: 0;
          padding: 0;
          top: 99%;
      }

    </style>

  </head>

<body <?php body_class(); ?>>
 
  <nav class="navbar navbar-default navbar-fixed-top guard-top">
    <div class="container bg-top-head">
    
      <div class="navbar-header">
        <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".js-navbar-collapse">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <!-- logo  --> 
          <?php if(empty(getOption('logo'))) : ?>
              <a class="navbar-brand" href="<?php echo site_url(); ?>"><?php echo bloginfo(); ?></a>
          <?php endif; ?>
          <?php if(!empty(getOption('logo'))) :?>
              <a href="<?php echo site_url(); ?>"><img src="<?php  echo getOption('logo'); ?>" width="180" class="logo"></a>
          <?php endif; ?>
      </div>
      <div class="collapse navbar-collapse js-navbar-collapse navbar-collapse-no-right"> 
        <?php
          wp_nav_menu( array(
              'menu'              => 'lpguardian-menu',
              'theme_location'    => 'lpguardian-menu',
              'depth'             => 2,
              'container'         => 'div',
              'container_class'   => 'collapse navbar-collapse',
              'container_id'      => 'bs-example-navbar-collapse-1',
              'menu_class'        => 'nav navbar-nav yamm',
              'fallback_cb'       => 'Yamm_Fw_Nav_Walker_menu_fallback',
              'walker'            => new Yamm_Fw_Nav_Walker())
          );
      ?>

      <div class="col-md-4 pull-right box-search no-getter">
      <form role="search" method="get" id="searchform" class="searchform" action="<?php echo esc_url( home_url(  SLUG_CUSTOME_POST.'/' ) ); ?>">
          <div>
              <label class="screen-reader-text" for="s"><?php _x( 'Search for:', 'label' ); ?></label>
              <input type="text" value="<?php echo get_search_query(); ?>" name="s" id="s" class="box-search-input" placeholder="Search..."/>
              <button type="submit" id="searchsubmit" class="btn search" />
              <i class="glyphicon glyphicon-search"></i> <?php //echo esc_attr_x( 'Search', 'submit button' ); ?> </button>
          </div>
      </form>
      </div>

      </div>

    </div>

      <!-- /.nav-collapse -->
  </nav>
    
    
