<?php get_header(); ?>
	<?php if (is_category( )) {?>
	 
    <div class="content-section-a" style="padding-top:60px;">

        <div class="container">
            <div class="row no-getter"> 
                <div class="col-md-12"><h3 class="heading">
                <?php 
					$cat = get_query_var('cat');
					$catName = get_category ($cat); 
					print_r($catName->name);
				  ?>
                </h3></div>

                  <?php if ( have_posts() ) : ?>
          		  <?php while ( have_posts() ) : the_post(); ?>
                   
                <div class="col-md-2 col-xs-4 box-thumb-images"> 
                    <div class="thumbnail no-border">
                        <a href="<?php echo get_permalink(); ?>"> 
                        <img class="img-responsive" src="<?php echo get_post_meta(get_the_ID(),'wpviddycpa-meta-image',TRUE) ?>">
                        <div class="caption-play">
                            <img src="http://mtdb.info/assets/images/play.png">
                        </div>
                        <div class="caption">  
                            <span class="label-no-radius label-viddy-black" rel="tooltip" title="IMDB Rate">
                                <i class="fa fa-star-o yellow"></i> <?php echo get_post_meta(get_the_ID(),'wpviddycpa-meta-rating',TRUE) ?>
                            </span>  
                        </div>
                        </a>
                    </div>
                    <div class="box-title-thumb">
                        <a href="<?php echo get_permalink(); ?>" class="text-left">
                            <h5><?php echo get_the_title(); ?></h5> 
                        </a>
                    </div>
                </div>
                <?php endwhile; ?>  
                <?php else : ?>
					<?php get_template_part( 'content', 'none' ); ?>
				 <?php endif; ?>
                
                <div class="col-md-12 pagination-guard">
                 <?php custom_pagination(); ?> 
                </div> 

           
                
            </div>

        </div>
        <!-- /.container -->

    </div>
    <!-- /.content-section-a -->

	<?php } ?> 
<?php get_footer(); ?>
          