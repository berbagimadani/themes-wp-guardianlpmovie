<?php get_header(); ?>

 <?php  
	$video_id   = get_post_meta(get_the_ID(),'wpviddycpa-meta-url_video',TRUE);
	$player     = get_post_meta(get_the_ID(),'wpviddycpa-meta-player',TRUE); 
	$url_image  = get_post_meta(get_the_ID(),'wpviddycpa-meta-url_image',TRUE);
	$type_cta   = get_post_meta(get_the_ID(),'wpviddycpa-meta-type_cta',TRUE);
	$title      = get_the_title();

	$url_cta   = get_post_meta(get_the_ID(),'wpviddycpa-meta-url_cta',TRUE);
	$locker    = get_post_meta(get_the_ID(),'wpviddycpa-meta-locker',TRUE);
	$url_referral    = get_post_meta(get_the_ID(),'wpviddycpa-meta-url_referral',TRUE);
	$duration_show_offer    = get_post_meta(get_the_ID(),'wpviddycpa-meta-duration_show_offer',TRUE);

	$show_title_offer =  !empty($options['show-title-offer']) ? $options['show-title-offer'] : '';

	$download_type_cta =   get_post_meta(get_the_ID(),'wpviddycpa-meta-download:type_cta',TRUE);
	$download_url_cta =   get_post_meta(get_the_ID(),'wpviddycpa-meta-download:url_cta',TRUE);
	$download_locker =  get_post_meta(get_the_ID(),'wpviddycpa-meta-download:locker',TRUE);
	$download_url_referral    = get_post_meta(get_the_ID(),'wpviddycpa-meta-download:url_referral',TRUE);

	$get_permalink = get_permalink(get_the_ID());
	$view_watch    = get_post_meta(get_the_ID(),'wpviddycpa-meta-view_watch',TRUE);
?> 
<?php if ( have_posts() ) : ?>
   
    <div class="intro-header">
        <div class="container">

            <span style="font-size:26px; color:#eee">
                <?php printf( __( 'Search Results for: %s', 'twentysixteen' ), '<span>' . esc_html( get_search_query() ) . '</span>' ); ?>
            </span> 

            <?php 
            // Start the loop.
            while ( have_posts() ) : the_post();
 
            ?>
            <div class="row main-background">
                <div class="col-md-2" style="text-align:left; padding-bottom:10px; padding-top:10px">
                    <a href="<?php echo get_permalink(); ?>"> 
                        <img src="<?php echo get_post_meta(get_the_ID(),'wpviddycpa-meta-image',TRUE); ?>" class="img-responsive-">
                    </a>
                </div>
                <div class="col-md-8">
                    <div class="main-desc" style="text-align:left">
                        <a href="<?php echo get_permalink(); ?>"><h3 class="titleviddy title-generate">
                             <?php echo get_post_meta(get_the_ID(),'wpviddycpa-meta-title',TRUE); ?> 
                        </h3> </a>
                        <p class="desc-viddy" id="description"><?php echo get_post_meta(get_the_ID(),'wpviddycpa-meta-description',TRUE); ?></p>

                        <div class="mvic-info">
                            <div class="mvici-left">
                                
                                <?php if(get_post_meta(get_the_ID(),'wpviddycpa-meta-genres',TRUE)) : ?>
                                <p><strong>Genre: </strong><a id="genres"><?php echo get_post_meta(get_the_ID(),'wpviddycpa-meta-genres',TRUE); ?></a></p>
                                <?php endif; ?>

                                <?php if(get_post_meta(get_the_ID(),'wpviddycpa-meta-actors',TRUE)) : ?>
                                <p><strong>Actor: </strong><a id="actors"><?php echo get_post_meta(get_the_ID(),'wpviddycpa-meta-actors',TRUE); ?></a></p> 
                                <?php endif; ?>
                                
                                <?php if(get_post_meta(get_the_ID(),'wpviddycpa-meta-duration',TRUE)) : ?>
                                    <p><strong>Duration:</strong> <span id="duration"><?php echo get_post_meta(get_the_ID(),'wpviddycpa-meta-duration',TRUE); ?></span></p>
                                <?php endif; ?>
                                <?php if(get_post_meta(get_the_ID(),'wpviddycpa-meta-release',TRUE)) : ?>
                                    <p><strong>Release:</strong> <span id="release"><?php echo get_post_meta(get_the_ID(),'wpviddycpa-meta-release',TRUE); ?></span></p>
                                <?php endif; ?>
                                <?php if(get_post_meta(get_the_ID(),'wpviddycpa-meta-rating',TRUE)) : ?>
                                    <p><strong>Rating IMDB:</strong> <span id="rating"><?php echo get_post_meta(get_the_ID(),'wpviddycpa-meta-rating',TRUE); ?></span></p>
                                <?php endif; ?>

                            </div> 
                               
                            <div class="clearfix"></div> 
                            <span id="view" style="display:none"></span> 

                        </div>
                    </div>    
                </div> 
            </div>
            <?php endwhile; ?>

            <div class="row">
	            <div class="col-md-12 pagination-guard"> 
	                   <?php
	                    $big = 999999999; // need an unlikely integer
	                    echo paginate_links( array(
	                        'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
	                        'format' => '?paged=%#%',
	                        'current' => max( 1, get_query_var('paged') ),
	                    ) );
	                   ?> 
	            </div>
            </div>

        </div>
        <!-- /.container -->

    </div>
    <!-- /.intro-header -->

<?php 
// If no content, include the "No posts found" template.
else :
?>        
    <div style="padding:10px; background:#fff">
        <div class="container">
            <div class="row"> 
                <div class="col-md-12">
                   <h2><?php _e( 'Sorry, but nothing matched your search terms. Please try again with some different keywords.' ); ?></h2>
                </div>
            </div>
        </div>
    </div>
 

<?php endif; ?>


<?php get_footer(); ?>
          