 <?php get_header(); ?>

 <?php global $wp_query; ?>
	<?php 
	
	if( !empty($wp_query->query_vars['taxonomy'])){	
		$slug_tax = $wp_query->query_vars[SLUG_TAXONOMY_POST];
	} 
	?>
    <?php
        $termby = get_term_by('slug', $slug_tax, SLUG_TAXONOMY_POST);  
    ?>
    <div class="content-section-a" style="padding-top:60px;">

        <div class="container">
            <div class="row no-getter"> 
                <div class="col-md-12"><h3 class="heading"><?php echo $termby->name;?></h3></div>

                <?php 

                   //$currentCategory = single_cat_title("", false);
                    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1; 
 
                    $args = array(
                        'post_type' => SLUG_CUSTOME_POST,
                         SLUG_TAXONOMY_POST => $slug_tax,
                        'paged'=> $paged,
                        'order' => 'DESC',
                        'posts_per_page' => '12'
                    );  
                    $the_query = new WP_Query( $args ); 
            if($the_query->have_posts()) :     

                    while ($the_query->have_posts()):
                        $the_query->the_post();   
                        $featured_image_array = wp_get_attachment_image_src( get_post_thumbnail_id(), 'single-post-thumbnail' );
                        $featured_image = $featured_image_array[0];
                   ?>  
                <div class="col-md-2 col-xs-4 box-thumb-images"> 
                    <div class="thumbnail no-border">
                        <a href="<?php echo get_permalink(); ?>"> 
                        <img class="img-responsive" src="<?php echo get_post_meta(get_the_ID(),'wpviddycpa-meta-image',TRUE) ?>">
                        <div class="caption-play">
                            <img src="http://mtdb.info/assets/images/play.png">
                        </div>
                        <div class="caption">  
                            <span class="label-no-radius label-viddy-black" rel="tooltip" title="IMDB Rate">
                                <i class="fa fa-star-o yellow"></i> <?php echo get_post_meta(get_the_ID(),'wpviddycpa-meta-rating',TRUE) ?>
                            </span>  
                        </div>
                        </a>
                    </div>
                    <div class="box-title-thumb">
                        <a href="<?php echo get_permalink(); ?>" class="text-left">
                            <h5><?php echo get_the_title(); ?></h5> 
                        </a>
                    </div>
                </div>
                <?php endwhile; ?> <?php wp_reset_query(); ?> 
                
                <div class="col-md-12 pagination-guard">
                <?php if ($the_query->max_num_pages > 1) { // check if the max number of pages is greater than 1  ?>
                   <?php
                    $big = 999999999; // need an unlikely integer
                    echo paginate_links( array(
                        'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
                        'format' => '?paged=%#%',
                        'current' => max( 1, get_query_var('paged') ),
                        'total' => $the_query->max_num_pages
                    ) );
                   ?>
                <?php } ?>

                <?php else: ?>
                  <article>
                    <h2>Sorry...</h2>
                    <p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
                  </article>
                </div>
            <?php endif; ?>

            <div class="wpviddy-ads1">
                 <?php echo getOption('ads-banner-single'); ?>
            </div>
                
            </div>

        </div>
        <!-- /.container -->

    </div>
    <!-- /.content-section-a -->

 <?php get_footer(); ?>