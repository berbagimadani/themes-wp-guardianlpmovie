<?php
/**
 * Template Name: Full Width Page
 *
 * @package WordPress
 * @subpackage lpmovieguardian
 * @since lpmovieguardian 2.0
 */

?>

<?php 
/**
* Gen Themes Display.
* @package WordPress 
* @subpackage Genthemes V1
* @since genthemes v1
* @web genthemes.net
* @email genthemes@gmail.com
*/
?>
<!doctype html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="<?php echo getOption('favicon]');?>">

    <title>
    <?php if ( is_category() ) {
	echo 'Category Archive for &quot;'; single_cat_title(); echo '&quot; | '; bloginfo( 'name' );
	} elseif ( is_tag() ) {
		echo 'Tag Archive for &quot;'; single_tag_title(); echo '&quot; | '; bloginfo( 'name' );
	} elseif ( is_archive() ) {
		wp_title(''); echo ' Archive | '; bloginfo( 'name' );
	} elseif ( is_search() ) {
		echo 'Search for &quot;'._wp_specialchars($s).'&quot; | '; bloginfo( 'name' );
	} elseif ( is_home() || is_front_page() ) {
		bloginfo( 'name' ); echo ' | '; bloginfo( 'description' );
	}  elseif ( is_404() ) {
		echo 'Error 404 Not Found | '; bloginfo( 'name' );
	} elseif ( is_single() ) {
		wp_title('');
	} else {
		echo wp_title( ' | ', false, 'right' ); bloginfo( 'name' );
	} ?>
    </title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo get_template_directory_uri(); ?>/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template --> 
    <link href="<?php echo get_template_directory_uri().'/css/landing-page.css'; ?>" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri(); ?>/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    
    <link rel="stylesheet" href="https://cdn.plyr.io/1.6.16/plyr.css"> 
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(). '/social-share/jssocials.css'; ?>">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(). '/social-share/jssocials-theme-flat.css'; ?>">

    <link rel="stylesheet" href="http://callmenick.com/_development/five-star-rating/css/rating.min.css">
     <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
    <script src="http://callmenick.com/_development/five-star-rating/js/dist/rating.min.js"></script>

    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  <?php wp_head(); ?> 
  <?php $root = get_template_directory_uri(); ?>

  <?php echo getOption('ads-scripts'); ?>

  <div id="fb-root"></div>
  <script>(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.7&appId=430707847037600";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));</script>


    <style type="text/css">
        <?php $options = get_option('wpviddycpa-option'); ?>
        .intro-header {
            padding-top: 20px; /* If you're making other pages, make sure there is 50px of padding to make sure the navbar doesn't overlap content! */
            padding-bottom: 50px;
            text-align: center;
            background: url("<?php echo @$options['background_one_page'];?>") no-repeat center center;
            background-size: cover;
        }
        .banner {
            padding: 100px 0;
            color: #f8f8f8;
            background: url("<?php echo @$options['banner_one_page'];?>") no-repeat center center;
            background-size: cover;
        }
        .main-background2{
            background-color: rgba(255,255,255,0.8); 
        }
        .bg-color-top-cst{
        	background:  <?php echo @$options['background-top-cst'];?>
        }
        .navbar-default.bg-color-top-cst{
        	background: <?php echo @$options['background-top-cst'];?>
        }
        .bg-color-top-cst .navbar-brand{
        	color: <?php echo @$options['color-top-cst'];?>
        }
        #menu-one-page-menu li a{
        	color: <?php echo @$options['color-top-cst'];?>
        }
        #menu-one-page-menu li{
         	background: <?php echo @$options['menu-color-top-cst'];?>;
         	margin-left: 10px;
         	margin-right: 10px;
        }
    </style> 
  </head>

  <body <?php body_class(); ?>>

  <?php
  $slug_custome_post =  !empty($options['slug-custome-post-type']) ? $options['slug-custome-post-type'] : '';
  $slug_taxonomy_post =  !empty($options['slug-taxonomy-post-type']) ? $options['slug-taxonomy-post-type'] : ''; 
  ?>
<div class="bg-top-head bg-color-top-cst">
  <div class="container">
  <nav class="navbar yamm navbar-default bg-color-top-cst">
    <div class="navbar-header">
      <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".js-navbar-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <!-- logo  --> 
        <?php if(empty(getOption('logo'))) : ?>
            <a class="navbar-brand" href="<?php echo site_url(); ?>"><?php echo bloginfo(); ?></a>
        <?php endif; ?>
        <?php if(!empty(getOption('logo'))) :?>
            <a href="<?php echo site_url(); ?>"><img src="<?php  echo getOption('logo'); ?>" width="180" class="logo"></a>
        <?php endif; ?>
    </div>
    <div class="collapse navbar-collapse js-navbar-collapse navbar-collapse-no-right"> 
      <?php
        wp_nav_menu( array(
            'menu'              => 'one-page-menu',
            'theme_location'    => 'one-page-menu',
            'depth'             => 2,
            'container'         => 'div',
            'container_class'   => 'collapse navbar-collapse',
            'container_id'      => 'bs-example-navbar-collapse-1',
            'menu_class'        => 'nav navbar-nav yamm',
            'fallback_cb'       => 'Yamm_Fw_Nav_Walker_menu_fallback',
            'walker'            => new Yamm_Fw_Nav_Walker())
        );
    ?>
    </div>

    <!-- /.nav-collapse -->
	</nav>
	</div>
</div>
  

<div class="intro-header">
        <div class="container">
            
            <div class="row main-background">
                <div class="col-lg-12 canvas-viddy-desc">
                    <?php 
                
                    $args_one = array( 'post_type' => SLUG_CUSTOME_POST,  'order' => 'DESC', 'posts_per_page' =>  1, 'post__in'  => get_option( 'sticky_posts' ),
    'ignore_sticky_posts' => 1);  
                    $the_query_one = new WP_Query( $args_one );   
                        while ($the_query_one->have_posts()):
                        $the_query_one->the_post();

                    ?>  
                    <?php $url_image = get_post_meta(get_the_ID(),'wpviddycpa-meta-url_image',TRUE); ?>
                    <?php
                    $video_id   = get_post_meta(get_the_ID(),'wpviddycpa-meta-url_video',TRUE);
					$player     = get_post_meta(get_the_ID(),'wpviddycpa-meta-player',TRUE); 
					$url_image  = get_post_meta(get_the_ID(),'wpviddycpa-meta-url_image',TRUE);
					$type_cta   = get_post_meta(get_the_ID(),'wpviddycpa-meta-type_cta',TRUE);
					$title      = get_the_title();

					$url_cta   = get_post_meta(get_the_ID(),'wpviddycpa-meta-url_cta',TRUE);
					$locker    = get_post_meta(get_the_ID(),'wpviddycpa-meta-locker',TRUE);
					$url_referral    = get_post_meta(get_the_ID(),'wpviddycpa-meta-url_referral',TRUE);
					$duration_show_offer    = get_post_meta(get_the_ID(),'wpviddycpa-meta-duration_show_offer',TRUE);

					$show_title_offer =  !empty($options['show-title-offer']) ? $options['show-title-offer'] : '';

					$download_type_cta =   get_post_meta(get_the_ID(),'wpviddycpa-meta-download:type_cta',TRUE);
					$download_url_cta =   get_post_meta(get_the_ID(),'wpviddycpa-meta-download:url_cta',TRUE);
					$download_locker =  get_post_meta(get_the_ID(),'wpviddycpa-meta-download:locker',TRUE);
					$download_url_referral    = get_post_meta(get_the_ID(),'wpviddycpa-meta-download:url_referral',TRUE);

					$get_permalink = get_permalink(get_the_ID());
					$view_watch    = get_post_meta(get_the_ID(),'wpviddycpa-meta-view_watch',TRUE);

				    $general_intro_video = getOption('general-intro-video');
				    $general_url_cpa = getOption('general-url-cpa');
                    ?>
                    <div class="intro-message" id="element">

                        <div id="player"></div> 
                        <div id="top-top" class="top-top"></div>
                        <div id="top-bottom" class="top-botttom"></div>
                        <div class="top-background">
                            <div class="videobox">
                            <a title="Lake Placid: The Final Chapter" class="thumb viddy-cover" id="play_btn_cover" style="background-image: url(<?php echo $url_image; ?>)"><span class="play-wrapper ease"><span id="play" class="fa fa-youtube-play ease"></span></span></a>
                            </div> 
                        </div>  
                         
                        <div class="media-controls">
                            <div id="leftControls">
                                <button type="button" name="Play" class="btn glyphicon glyphicon-play" id="play_btn"></button>
                                <button id="volumeInc_btn" name="Volume" class="btn glyphicon glyphicon-volume-up"></button>
                                <?php if( empty(get_post_meta(get_the_ID(),'wpviddycpa-meta-duration',TRUE)) ) { ?>
                                <button id="timeContainer" class="btn">0:00:<span class="timer">00</span></button>
                                <?php } else { ?>
                                <button id="timeContainer" class="btn"><?php echo gmdate('H:i:s', get_post_meta(get_the_ID(),'wpviddycpa-meta-duration',TRUE)*60);?></button>
                                <?php } ?>
                            </div>
                            <div id="rightControls">
                                <div id="sliderContainer">
                                    <div id="slider" class="ui-slider ui-slider-vertical ui-widget ui-widget-content ui-corner-all">
                                        <div class="ui-slider-range ui-widget-header ui-corner-all ui-slider-range-min" style="height: 50%;"></div>
                                        <span class="ui-slider-handle ui-state-default ui-corner-all" tabindex="0" style="bottom: 50%;"></span>
                                    </div>
                                </div>
                                <div id="setting_btn" class="btn-group dropup">
                                    <span class="glyphicon glyphicon-hd-video"></span> 
                                </div>
                                <button id="fullscreen_btn" name="Fullscreen" class="btn glyphicon glyphicon-resize-full"></button> 
                            </div>
                        </div>
                    </div>

                    <!--  other server -->
                    <div class="server-video" style="text-align:left">
                        <div class="col-md-12">
                            <div class="row server-line">
                                <div class="col-md-3"><a href="#" data-target="#server-download" data-toggle="modal" class="server-downlad"> <span class="glyphicon glyphicon-tasks" aria-hidden="true"></span> Free Download <?php echo $title; ?></a></div>
                                <div class="col-md-3"><button class="btn btn-sm btn-default server-active"> <span class="glyphicon glyphicon-play" aria-hidden="true"></span> HD</button></div>
                            </div>
                            <div class="row server-line">
                                <div class="col-md-3"><a href="" data-target="#server-download" data-toggle="modal" class="server-downlad"> <span class="glyphicon glyphicon-tasks" aria-hidden="true"></span> Free Download</a></div>
                                <div class="col-md-3"><button class="btn btn-sm btn-default" id="play-server"> <span class="glyphicon glyphicon-play" aria-hidden="true"></span> CAM</button></div>
                            </div>
                            <div class="row server-line">
                                <div class="col-md-3"><a href="" data-target="#server-download" data-toggle="modal" class="server-downlad"> <span class="glyphicon glyphicon-tasks" aria-hidden="true"></span> <?php echo $title; ?> Full</a></div>
                                <div class="col-md-3"><button class="btn btn-sm btn-default" id="play-server2"><span class="glyphicon glyphicon-play" aria-hidden="true"></span> FULL HD</button></div>
                            </div>
                        </div>
                    </div>
                    <!--  // other server -->

                    <div class="row main-background">
                <?php 
                    $ex_url_cpa = explode(';',  $general_url_cpa);
                    $filter_url_cpa = array_filter($ex_url_cpa);  
                    $cpa_ran = array_rand($filter_url_cpa); 
                ?>
                <div class="col-md-2" style="text-align:left; padding-bottom:10px;">
                    <a href="<?php echo $get_permalink; ?>"> 
                        <img src="<?php echo get_post_meta(get_the_ID(),'wpviddycpa-meta-image',TRUE); ?>" class="img-responsive">
                    </a>
                </div>
                <div class="col-md-7">
                    <div class="main-desc" style="text-align:left">
                        <h3 class="titleviddy title-generate"><?php echo $title; ?></h3> 

                        <div class="block-trailer">
                            <a data-target="#pop-trailer" data-toggle="modal" class="btn btn-primary">
                                <i class="fa fa-video-camera mr5"></i>Trailer
                            </a>
                        </div>

                        <!-- Modal Trailer-->
                        <div class="modal fade" id="pop-trailer" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                          <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-video-camera mr5"></i>Trailer :: <?php echo $title; ?></h4>
                              </div>
                              <div class="modal-body">
                                <iframe title="YouTube video player" class="youtube-player" type="text/html" 
width="100%" height="420" src="http://www.youtube.com/embed/<?php echo $video_id; ?>"
frameborder="0" allowFullScreen></iframe> 
                              </div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> 
                              </div>
                            </div>
                          </div>
                        </div>

                        <p class="desc-viddy" id="description"><?php echo get_the_content(); ?></p>

                        <div class="mvic-info">
                            <div class="mvici-left">
                                
                                <?php if(get_post_meta(get_the_ID(),'wpviddycpa-meta-genres',TRUE)) : ?>
                                <p><strong>Genre: </strong><a id="genres"><?php echo get_post_meta(get_the_ID(),'wpviddycpa-meta-genres',TRUE); ?></a></p>
                                <?php endif; ?>

                                <?php if(get_post_meta(get_the_ID(),'wpviddycpa-meta-actors',TRUE)) : ?>
                                <p><strong>Actor: </strong><a id="actors"><?php echo get_post_meta(get_the_ID(),'wpviddycpa-meta-actors',TRUE); ?></a></p> 
                                <?php endif; ?>
                                
                                <?php if(get_post_meta(get_the_ID(),'wpviddycpa-meta-duration',TRUE)) : ?>
                                    <p><strong>Duration:</strong> <span id="duration"><?php echo get_post_meta(get_the_ID(),'wpviddycpa-meta-duration',TRUE); ?></span></p>
                                <?php endif; ?>
                                <?php if(get_post_meta(get_the_ID(),'wpviddycpa-meta-release',TRUE)) : ?>
                                    <p><strong>Release:</strong> <span id="release"><?php echo get_post_meta(get_the_ID(),'wpviddycpa-meta-release',TRUE); ?></span></p>
                                <?php endif; ?>
                                <?php if(get_post_meta(get_the_ID(),'wpviddycpa-meta-rating',TRUE)) : ?>
                                    <p><strong>Rating IMDB:</strong> <span id="rating"><?php echo get_post_meta(get_the_ID(),'wpviddycpa-meta-rating',TRUE); ?></span></p>
                                <?php endif; ?>
                                 <?php if(get_post_meta(get_the_ID(),'wpviddycpa-meta-duration',TRUE)) : ?>
                                    <p><strong>Duration:</strong> <span id="rating"><?php echo get_post_meta(get_the_ID(),'wpviddycpa-meta-duration',TRUE); ?> minutes</span></p>
                                <?php endif; ?>

                            </div> 
                               
                            <div class="clearfix"></div>
                            <div id="share"></div>
                            <span id="view" style="display:none"></span> 

                        </div>
                    </div>    
                </div>
                <div class="col-md-3"> 
 
                    <div class="box-button-optional">
                        <span class="btn-download-viddy"></span> 
                    </div>
                    <div class="box-button-optional">
                        <span class="btn-download-viddy"></span> 
                    </div>
                    <div class="box-button-optional">
                        <div class="mv-rating"><div id="movie-mark" class="btn btn-danger"><?php echo get_post_meta(get_the_ID(),'wpviddycpa-meta-rating',TRUE); ?></div>
                        <label id="movie-rating">IMDB</label>

                        <ul class="c-rating"></ul>
                        <?php $rt = get_post_meta(get_the_ID(),'wpviddycpa-meta-rating',TRUE) / 2; ?>
                        <script type="text/javascript"> 
                            $( document ).ready(function() {
                                var ratingElement =  document.querySelector('.c-rating');
                                var currentRating = '<?php echo $rt; ?>';
                                var maxRating = 5;
                                var callback = function(rating) { alert(rating); };
                                var r = rating(ratingElement, currentRating, maxRating, callback); 

                            });                           
                        </script>
                        <div class="clearfix"></div> 
                        </div>
                    </div>

                    <div class="box-button-optional">
                        <a data-target="#pop-download" data-toggle="modal" data-backdrop="static" data-keyboard="false" class="btn btn-block btn-lg btn-successful btn-01 pop-btn-download">
                        <i class="fa fa-download mr10"></i>
                            Download in HD
                        </a>
                    </div>
                    <div class="box-button-optional">
                        <a data-target="#pop-download" data-toggle="modal" data-backdrop="static" data-keyboard="false" class="btn btn-block btn-lg btn-successful btn-01 pop-btn-download"><i class="fa fa-play mr10"></i>
                            Stream in HD
                        </a>
                    </div>

                        <!-- Modal -->
                        <div class="modal fade" id="pop-download" tabindex="-1" role="dialog" aria-labelledby="pop-downloadLabel">
                          <div class="modal-dialog register-full-movie-lg" role="document">
                            <div class="modal-content register-full-movie"> 
                             
                             <div class="top-content" style="background-image:url(<?php echo $url_image; ?>)">
                                <p class="text-center top"><?php echo $title; ?></p><p class="text-center bottom">Released Date: <?php echo get_post_meta(get_the_ID(),'wpviddycpa-meta-release',TRUE); ?></p>
                             </div>
                             <div class="bottom-content"><img class="img-responsive" src="http://www.movioz.co/wp-content/themes/magelo/img/offer.png" width="614" height="275">
                             <p class="text-center"><span class="btn btn-offer btn-primary btn-lg" data-title="<?php echo $title; ?>">Register Free Account</span></p>
                             </div>

                            </div>
                          </div>
                        </div>

                        <!-- Modal -->
                        <div class="modal fade" id="server-download" tabindex="-1" role="dialog" aria-labelledby="pop-downloadLabel">
                          <div class="modal-dialog register-full-movie-lg" role="document">
                            <div class="modal-content register-full-movie"> 
                             
                             <div class="top-content" style="background-image:url(<?php echo $url_image; ?>)">
                                <p class="text-center top"><?php echo $title; ?></p><p class="text-center bottom">Released Date: <?php echo get_post_meta(get_the_ID(),'wpviddycpa-meta-release',TRUE); ?></p>
                             </div>
                             <div class="bottom-content"><img class="img-responsive" src="http://www.movioz.co/wp-content/themes/magelo/img/offer.png" width="614" height="275">
                             <p class="text-center"><span class="btn btn-offer btn-primary btn-lg" data-title="<?php echo $title; ?>">Register Free Account</span></p>
                             </div>

                            </div>
                          </div>
                        </div>

                </div>

            </div>

        </div>
        <!-- /.container -->

                    <!-- Modal -->
                        <div class="modal fade" id="pop-download" tabindex="-1" role="dialog" aria-labelledby="pop-downloadLabel">
                          <div class="modal-dialog register-full-movie-lg" role="document">
                            <div class="modal-content register-full-movie"> 
                             
                             <div class="top-content" style="background-image:url(<?php echo $url_image; ?>)">
                                <p class="text-center top"><?php echo $title; ?></p><p class="text-center bottom">Released Date: <?php echo get_post_meta(get_the_ID(),'wpviddycpa-meta-release',TRUE); ?></p>
                             </div>
                             <div class="bottom-content"><img class="img-responsive" src="http://www.movioz.co/wp-content/themes/magelo/img/offer.png" width="614" height="275">
                             <p class="text-center"><span class="btn btn-offer btn-primary btn-lg" data-title="<?php echo $title; ?>">Register Free Account</span></p>
                             </div>

                            </div>
                          </div>
                        </div>
                        
                    <?php endwhile; ?>
                </div>
            </div>

        </div>
        <!-- /.container -->

    </div>
    <!-- /.intro-header -->

    <!-- Page Content -->
	<div class="container">
        <div class="row no-getter"> 
            <div class="col-md-12 fb-comment"> 
                <div class="fb-comments" data-href="<?php echo $get_permalink; ?>" data-width="100%" data-numposts="5"></div>
            </div>
        </div>
    </div>
    <div class="content-section-a">

        <div class="container">
            <div class="row no-getter"> 
                <div class="col-md-12">
                    <h3>You May Also Like</h3>   
                </div>
                 <?php  
                   //$currentCategory = single_cat_title("", false);
                    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                    $args = array(
                        'post_type' => SLUG_CUSTOME_POST,
                        //'gallery_category' => 'video-gallery',
                        'paged'=> $paged,
                        'order' => 'DESC',
                        'posts_per_page' => '12'
                    );  
                    $the_query = new WP_Query( $args ); 
            if($the_query->have_posts()) :     

                    while ($the_query->have_posts()):
                        $the_query->the_post();   
                        $featured_image_array = wp_get_attachment_image_src( get_post_thumbnail_id(), 'single-post-thumbnail' );
                        $featured_image = $featured_image_array[0];
                   ?>  
                <div class="col-md-2 col-xs-4 box-thumb-images"> 
                    <div class="thumbnail no-border">
                        <a href="<?php echo get_permalink(); ?>">
                        <div class="caption">
                            <h5><?php echo get_post_meta(get_the_ID(),'wpviddycpa-meta-title',TRUE) ?> </h5>  
                            <p style="font-size:11px">
                             <?php 
                                $text = get_post_meta(get_the_ID(),'wpviddycpa-meta-description',TRUE);
                                echo wp_trim_words( $text, $num_words = 18, $more = null );
                              ?> 
                            </p>
                            <p>
                            <span class="label label-danger" rel="tooltip" title="Release year"><?php echo get_post_meta(get_the_ID(),'wpviddycpa-meta-release',TRUE) ?> </span>
                            </p>
                            <p>
                            <span class="label label-viddy-orange" rel="tooltip" title="IMDB Rate">
                            IMDb:
                            <?php echo get_post_meta(get_the_ID(),'wpviddycpa-meta-rating',TRUE) ?>
                            </span>
                            </p>
                        </div>
                        <img class="img-responsive" src="<?php echo get_post_meta(get_the_ID(),'wpviddycpa-meta-image',TRUE) ?>">
                        </a>
                    </div>
                    <a href="<?php echo get_permalink(); ?>" class="text-center">
                        <h5><?php echo get_post_meta(get_the_ID(),'wpviddycpa-meta-title',TRUE) ?> </h5> 
                    </a>
                </div>
                <?php endwhile; ?>
                
                <div class="col-md-12">
                <?php if ($the_query->max_num_pages > 1) { // check if the max number of pages is greater than 1  ?>
                   <?php
                   /* $big = 999999999; // need an unlikely integer
                    echo paginate_links( array(
                        'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
                        'format' => '?paged=%#%',
                        'current' => max( 1, get_query_var('paged') ),
                        'total' => $the_query->max_num_pages
                    ) );*/
                   ?>
                <?php } ?>

                <?php else: ?>
                  <article>
                    <h2>Sorry...</h2>
                    <p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
                  </article>
                </div>
            <?php endif; ?>
            
            <div class="wpviddy-ads1">
                <?php echo getOption('ads-banner-single'); ?>
            </div>

            </div>
        </div>
        <!-- /.container -->

    </div>
    <!-- /.content-section-a -->

<?php 
get_footer();
