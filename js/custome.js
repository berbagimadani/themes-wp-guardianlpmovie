/*
* PLayer control
*/
var controls = ["<div class='plyr__controls'>",
    /*"<button type='button' data-plyr='restart'>",
        "<svg><use xlink:href='#plyr-restart'></use></svg>",
        "<span class='plyr__sr-only'>Restart</span>",
    "</button>",
    "<button type='button' data-plyr='rewind'>",
        "<svg><use xlink:href='#plyr-rewind'></use></svg>",
        "<span class='plyr__sr-only'>Rewind {seektime} secs</span>",
    "</button>",
    "<button type='button' data-plyr='play'>",
        "<svg><use xlink:href='#plyr-play'></use></svg>",
        "<span class='plyr__sr-only'>Play</span>",
    "</button>",
    "<button type='button' data-plyr='pause'>",
        "<svg><use xlink:href='#plyr-pause'></use></svg>",
        "<span class='plyr__sr-only'>Pause</span>",
    "</button>",
    "<button type='button' data-plyr='fast-forward'>",
        "<svg><use xlink:href='#plyr-fast-forward'></use></svg>",
        "<span class='plyr__sr-only'>Forward {seektime} secs</span>",
    "</button>",
    "<span class='plyr__progress'>",
        "<label for='seek{id}' class='plyr__sr-only'>Seek</label>",
        "<input id='seek{id}' class='plyr__progress--seek' type='range' min='0' max='100' step='0.1' value='0' data-plyr='seek'>",
        "<progress class='plyr__progress--played' max='100' value='0' role='presentation'></progress>",
        "<progress class='plyr__progress--buffer' max='100' value='0'>",
            "<span>0</span>% buffered",
        "</progress>",
        "<span class='plyr__tooltip'>00:00</span>",
    "</span>",*/
    "<span class='plyr__time' style='display:none'>",
        "<span class='plyr__sr-only'>Current time</span>",
        "<span class='plyr__time--current'>00:00</span>",
    "</span>",/*
    "<span class='plyr__time'>",
        "<span class='plyr__sr-only'>Duration</span>",
        "<span class='plyr__time--duration'>00:00</span>",
    "</span>",
    "<button type='button' data-plyr='mute'>",
        "<svg class='icon--muted'><use xlink:href='#plyr-muted'></use></svg>",
        "<svg><use xlink:href='#plyr-volume'></use></svg>",
        "<span class='plyr__sr-only'>Toggle Mute</span>",
    "</button>",
    "<span class='plyr__volume'>",
        "<label for='volume{id}' class='plyr__sr-only'>Volume</label>",
        "<input id='volume{id}' class='plyr__volume--input' type='range' min='0' max='10' value='5' data-plyr='volume'>",
        "<progress class='plyr__volume--display' max='10' value='0' role='presentation'></progress>",
    "</span>",
    "<button type='button' data-plyr='captions'>",
        "<svg class='icon--captions-on'><use xlink:href='#plyr-captions-on'></use></svg>",
        "<svg><use xlink:href='#plyr-captions-off'></use></svg>",
        "<span class='plyr__sr-only'>Toggle Captions</span>",
    "</button>", */
    "<button type='button' data-plyr='fullscreen'>",
        "<svg class='icon--exit-fullscreen'><use xlink:href='#plyr-exit-fullscreen'></use></svg>",
        "<svg><use xlink:href='#plyr-enter-fullscreen'></use></svg>",
        "<span class='plyr__sr-only'>Toggle Fullscreen</span>",
    "</button>",
"</div>"].join("");



$( document ).ready(function() { 
 

    var showData = $('#show-data');

    $.getJSON('data/data.php', function (data) { 

        /* bg image */
        $( "<div/>", {
            "class": "videobox",
            html: imageSrc(data.url_image, data.title)
        }).appendTo( "#overlay" );

        /* player video */
        $( "<div/>", {
            "class": "plyr",
            "id" : "video",
            html: typeVideo(data.player, data.url_video)
        }).appendTo( "#show_data_video" );


        if (data.player == 'youtube' || data.player == 'mp4') {

            plyr.setup('.plyr', {
                html: controls,
                //clickToPlay:false
                autoplay: false,
            });

            var x = document.getElementsByClassName("plyr__time--current");
            var v = $('#video');
            var pic1 = $('.btn-play');
            var pic2 = $('.btn-stop');
            var player1 = plyr.setup('.plyr')[0];

            v.bind('playing', function(x) { 
                //console.log('xx');
            });
            v.bind('timeupdate', function(y) {
                //player1.play(); 
                //console.log(y);
                //console.log(x[0].innerText);
                document.getElementById("view").innerHTML = x[0].innerText;

                var hms = '00:'+x[0].innerText; 
                var a = hms.split(':');
                var seconds = (+a[0]) * 60 * 60 + (+a[1]) * 60 + (+a[2]); 
                console.log(seconds);
                if(seconds > 12 ){
                     
                    player1.pause(); 

                }
                //console.log(num +'===='+ x[0].innerText)
            });
            v.bind('progress', function(x) {
                //player1.play(); 
                //console.log(x);
            }); 
        }

        /* */
        parsingTitle('title-generate', data.title);
        parsingText('title', data.title);
        parsingText('description', data.description);
        parsingImages('image', data.image);
        parsingText('genres', data.genres);
        parsingText('actors', data.actors);
        parsingText('duration', data.duration);
        parsingText('rating', data.rating);
        parsingText('release', data.release);
    });
 
});

function typeVideo(type, key) {

    switch (type) { 
       
        case "openload":  
        var html = ' <iframe src="'+key+'" width="100%" height="500" frameborder="0" id="video"></iframe>'; 
        return html;
        break;

        case "youtube":  
        var html = '<div data-video-id="'+key+'" data-type="youtube"></div>'; 
        return html;
        break;
                 

        default: 
            alert('Default case');
    }
}

function imageSrc(images, text) {
    var html = '<a title="'+text+'" class="thumb viddy-cover play-viddy-123" data="openLink" style="background-image: url('+images+')"><span class="viddy-view-dummy"><i class="fa fa-eye mr10"></i>115954</span></a>';
    //var html = '<img src="'+images+'" class="thumb viddy-cover"><span class="play-viddy-123" data="openLink"></span>';
    return html;
}

function parsingTitle(id, text) {
    var print = $('.'+id).html(text);
    return print;
}

function parsingText(id, text) {
    var print = $('#'+id).html(text);
    return print;
}

function parsingImages(id, text) {
    var images = '<img src="'+text+'" class="img-responsive">';
    var print = $('#'+id).html(images);
    return print;
}

/*
* HOVER
*/
$( document ).ready(function() {
        $("[rel='tooltip']").tooltip();    
     
        $('.thumbnail').hover(
            function(){
                $(this).find('.caption').slideDown(250); //.fadeIn(250)
            },
            function(){
                $(this).find('.caption').slideUp(250); //.fadeOut(205)
            }
        ); 
});

/*
* PopUp
*/

var gallerySettings = {
                markup      : '' +
                    '<div class="popup">' +
                        '<div class="popup_wrap">' +
                            '<h2>If youre curious about what people look for in makeup produ</h2>'+
                            '<h2>cts and how they shop for makeup, send a survey. Our expert-certified </h2>' +
                            '<div class="popup_content"/>' +
                        '</div>' +
                    '</div>',
                // This is a custom variable
                gallery     : '.default_popup'

            };
    $(function(){ 
        $('.default_popup').popup(gallerySettings);
});

/*
* PLAYER
*/
    
  
   