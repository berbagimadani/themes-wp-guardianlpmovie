<?php if ( is_home() ) { ?>
 
<?php } ?>

<?php if ( is_page() ) { ?>
<div class="container">
    <div class="row main-background">
        <div class="col-md-12">
            <h2><?php echo the_title(); ?></h2>

            <p>
            <?php echo the_content(); ?>
            </p>
        </div>
    </div>
</div>
<?php } ?>


<?php if ( is_single() ) { ?> 
	<?php  
	$video_id   = get_post_meta(get_the_ID(),'wpviddycpa-meta-url_video',TRUE);
	$player     = get_post_meta(get_the_ID(),'wpviddycpa-meta-player',TRUE); 
	$url_image  = get_post_meta(get_the_ID(),'wpviddycpa-meta-url_image',TRUE);
	$type_cta   = get_post_meta(get_the_ID(),'wpviddycpa-meta-type_cta',TRUE);
	$title      = get_the_title();

	$url_cta   = get_post_meta(get_the_ID(),'wpviddycpa-meta-url_cta',TRUE);
	$locker    = get_post_meta(get_the_ID(),'wpviddycpa-meta-locker',TRUE);
	$url_referral    = get_post_meta(get_the_ID(),'wpviddycpa-meta-url_referral',TRUE);
	$duration_show_offer    = get_post_meta(get_the_ID(),'wpviddycpa-meta-duration_show_offer',TRUE);

	$show_title_offer =  !empty($options['show-title-offer']) ? $options['show-title-offer'] : '';

	$download_type_cta =   get_post_meta(get_the_ID(),'wpviddycpa-meta-download:type_cta',TRUE);
	$download_url_cta =   get_post_meta(get_the_ID(),'wpviddycpa-meta-download:url_cta',TRUE);
	$download_locker =  get_post_meta(get_the_ID(),'wpviddycpa-meta-download:locker',TRUE);
	$download_url_referral    = get_post_meta(get_the_ID(),'wpviddycpa-meta-download:url_referral',TRUE);

	$get_permalink = get_permalink(get_the_ID());
	$view_watch    = get_post_meta(get_the_ID(),'wpviddycpa-meta-view_watch',TRUE);

    $general_intro_video = getOption('general-intro-video');
    $general_url_cpa = getOption('general-url-cpa');

	
    ?>

    <!--<div  style="padding:10px; background:#fff">
        <div class="container">
            <div class="row"> 
                <div class="col-md-12">
                    <span style="font-size:26px"><?php echo get_the_title(); ?></span> 
                </div>
            </div>
        </div>
    </div>-->

    <!-- Header -->
    <a name="about"></a>
    <div class="intro-header">
        <div class="container">

            <div class="row main-background-player">
                <div class="col-lg-12 canvas-viddy-desc">
                    <!-- PLAYER -->
                    <div class="intro-message" id="element"> 
                        <div id="player"></div> 
                        <div id="top-top" class="top-top"></div>
                        <div id="top-bottom" class="top-botttom"></div>
                        <div class="top-background">
                            <div class="videobox">
                            <a title="Lake Placid: The Final Chapter" class="thumb viddy-cover" id="play_btn_cover" style="background-image: url(<?php echo $url_image; ?>)"><span class="play-wrapper ease"><span id="play" class="fa fa-youtube-play ease"></span></span></a>
                            </div> 
                        </div>  
                        
                        <div class="media-controls">
                            <div id="leftControls">
                                <button type="button" name="Play" class="btn glyphicon glyphicon-play" id="play_btn"></button>
                                <button id="volumeInc_btn" name="Volume" class="btn glyphicon glyphicon-volume-up"></button>
                                
                                <?php if( empty(get_post_meta(get_the_ID(),'wpviddycpa-meta-duration',TRUE)) ) { ?>
                                <button id="timeContainer" class="btn">0:00:<span class="timer">00</span></button>
                                <?php } else { ?>
                                <button id="timeContainer" class="btn"><?php echo gmdate('H:i:s', get_post_meta(get_the_ID(),'wpviddycpa-meta-duration',TRUE)*60);?></button>
                                <?php } ?>
                            </div>
                            <div id="rightControls">
                                <div id="sliderContainer">
                                    <div id="slider" class="ui-slider ui-slider-vertical ui-widget ui-widget-content ui-corner-all">
                                        <div class="ui-slider-range ui-widget-header ui-corner-all ui-slider-range-min" style="height: 50%;"></div>
                                        <span class="ui-slider-handle ui-state-default ui-corner-all" tabindex="0" style="bottom: 50%;"></span>
                                    </div>
                                </div>
                                <div id="setting_btn" class="btn-group dropup">
                                    <span class="glyphicon glyphicon-hd-video"></span> 
                                </div>
                                <button id="fullscreen_btn" name="Fullscreen" class="btn glyphicon glyphicon-resize-full"></button> 
                            </div>
                        </div> 
                    </div>
                    <!-- END PLAYER -->
                    
                    <!--  other server 
                    <div class="server-video" style="text-align:left">
                        <div class="col-md-12">
                            <div class="row server-line">
                                <div class="col-md-3"><a href="#" data-target="#server-download" data-toggle="modal" class="server-downlad"> <span class="glyphicon glyphicon-tasks" aria-hidden="true"></span> Free Download <?php echo $title; ?></a></div>
                                <div class="col-md-3"><button class="btn btn-sm btn-default server-active"> <span class="glyphicon glyphicon-play" aria-hidden="true"></span> HD</button></div>
                            </div>
                            <div class="row server-line">
                                <div class="col-md-3"><a href="" data-target="#server-download" data-toggle="modal" class="server-downlad"> <span class="glyphicon glyphicon-tasks" aria-hidden="true"></span> Free Download</a></div>
                                <div class="col-md-3"><button class="btn btn-sm btn-default" id="play-server"> <span class="glyphicon glyphicon-play" aria-hidden="true"></span> CAM</button></div>
                            </div>
                            <div class="row server-line">
                                <div class="col-md-3"><a href="" data-target="#server-download" data-toggle="modal" class="server-downlad"> <span class="glyphicon glyphicon-tasks" aria-hidden="true"></span> <?php echo $title; ?> Full</a></div>
                                <div class="col-md-3"><button class="btn btn-sm btn-default" id="play-server2"><span class="glyphicon glyphicon-play" aria-hidden="true"></span> FULL HD</button></div>
                            </div>
                        </div>
                    </div>-->
                    <!--  // other server -->
                </div>
            </div> 

            <div class="row">
                <?php 
                    $ex_url_cpa = explode(';',  $general_url_cpa);
                    $filter_url_cpa = array_filter($ex_url_cpa);    
                ?>
                <div class="col-md-9 main-background-rev3">
                    <div class="row">
                        <div class="col-md-3" style="text-align:left; padding-bottom:10px;">
                            <a href="<?php echo $get_permalink; ?>"> 
                                <img src="<?php echo get_post_meta(get_the_ID(),'wpviddycpa-meta-image',TRUE); ?>" class="img-responsive">
                            </a>
                        </div>
                        <div class="col-md-9">
                            <div class="main-desc" style="text-align:left">
                                <h3 class="titleviddy title-generate"><?php echo $title; ?></h3> 

                                <div class="block-trailer">
                                    <a data-target="#pop-trailer" data-toggle="modal" class="btn btn-primary">
                                        <i class="fa fa-video-camera mr5"></i>Trailer
                                    </a>
                                </div>

                                <!-- Modal Trailer-->
                                <div class="modal fade" id="pop-trailer" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                  <div class="modal-dialog modal-lg" role="document">
                                    <div class="modal-content">
                                      <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="myModalLabel"><i class="fa fa-video-camera mr5"></i>Trailer :: <?php echo $title; ?></h4>
                                      </div>
                                      <div class="modal-body">
                                        <iframe title="YouTube video player" class="youtube-player" type="text/html" width="100%" height="420" src="http://www.youtube.com/embed/<?php echo $video_id; ?>" frameborder="0" allowFullScreen></iframe> 
                                      </div>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> 
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <p class="desc-viddy" id="description"><?php echo get_the_content(); ?></p>

                                <div class="mvic-info">
                                    <div class="mvici-left">
                                        
                                        <?php if(get_post_meta(get_the_ID(),'wpviddycpa-meta-genres',TRUE)) : ?>
                                        <p><strong>Genre: </strong><a id="genres"><?php echo get_post_meta(get_the_ID(),'wpviddycpa-meta-genres',TRUE); ?></a></p>
                                        <?php endif; ?>
                                        <!--
                                        <?php if(get_post_meta(get_the_ID(),'wpviddycpa-meta-actors',TRUE)) : ?>
                                        <p><strong>Actor: </strong><a id="actors"><?php echo get_post_meta(get_the_ID(),'wpviddycpa-meta-actors',TRUE); ?></a></p> 
                                        <?php endif; ?>-->

                                       
                                         
                                        <?php if(get_post_meta(get_the_ID(),'wpviddycpa-meta-release',TRUE)) : ?>
                                            <p><strong>Release:</strong> <span id="release"><?php echo get_post_meta(get_the_ID(),'wpviddycpa-meta-release',TRUE); ?></span></p>
                                        <?php endif; ?>
                                        <?php if(get_post_meta(get_the_ID(),'wpviddycpa-meta-rating',TRUE)) : ?>
                                            <p><strong>Rating IMDB:</strong> <span id="rating"><?php echo get_post_meta(get_the_ID(),'wpviddycpa-meta-rating',TRUE); ?></span></p>
                                        <?php endif; ?>
                                         <?php if(get_post_meta(get_the_ID(),'wpviddycpa-meta-duration',TRUE)) : ?>
                                            <p><strong>Duration:</strong> <span id="rating"><?php echo get_post_meta(get_the_ID(),'wpviddycpa-meta-duration',TRUE); ?> minutes</span></p>
                                        <?php endif; ?>

                                    </div> 
                                       
                                    <div class="clearfix"></div>
                                    <div id="share"></div>
                                    <span id="view" style="display:none"></span> 

                                </div>
                            </div>    
                        </div>
                        <!-- tab additional content -->
                        <div class="col-md-12 tab-margin">
                            <div class="tabs-lp-guard" data-example-id="togglable-tabs"> 
                                <ul class="nav nav-tabs" id="myTabs" role="tablist"> 
                                    <li role="presentation" class="active">
                                        <a href="#gallery" id="gallery-tab" role="tab" data-toggle="tab" aria-controls="gallery" aria-expanded="true">Gallery</a>
                                    </li> 
                                    <li role="presentation" class="">
                                        <a href="#actors" role="tab" id="actors-tab" data-toggle="tab" aria-controls="actors" aria-expanded="false">Actors</a>
                                    </li>
                                    <li role="presentation" class="">
                                        <a href="#trailer" role="tab" id="trailer-tab" data-toggle="tab" aria-controls="trailer" aria-expanded="false">Trailer</a>
                                    </li>  
                                </ul> 
                                <div class="tab-content" id="myTabContent"> 
                                    <!-- Gallery  -->
                                    <div class="tab-pane fade active in" role="tabpanel" id="gallery" aria-labelledby="gallery-tab">
                                       <div class="row" style="padding:10px;">

                                             <?php 
                                            $img_gallery = json_decode( get_post_meta(get_the_ID(),'wpviddycpa-meta-image_gallery',TRUE) ); 
                                            foreach ($img_gallery as $key => $value) {  
                                            ?>

                                            <div class="col-md-3 col-xs-4 box-thumb-images-tab"> 
                                                <div class="thumbnail no-border"> 
                                                    <img class="img-responsive" src="http://image.tmdb.org/t/p/w185<?php echo @$value; ?>"> 
                                                </div> 
                                            </div> 
                                            <?php } ?>

                                       </div>
                                    </div> 
                                    <!-- End Gallery  --> 
                                     <!-- Actors -->
                                    <div class="tab-pane fade" role="tabpanel" id="actors" aria-labelledby="actors-tab"> 
                                        <div class="row" style="padding:10px;">
                                            <?php 
                                            $img_actor = json_decode( get_post_meta(get_the_ID(),'wpviddycpa-meta-image_actors',TRUE) ); 
                                            foreach ($img_actor as $key => $value) {   
                                                $data = @$value->datas; 
                                            ?>
                                            <div class="col-md-2 col-xs-2 box-thumb-images-tab"> 
                                                <div class="thumbnail no-border">  
                                                    <img class="img-responsive" src="http://image.tmdb.org/t/p/w185<?php echo @$data->profile_path; ?>" alt="<?php echo @$data->name;?>"> 
                                                    <h6><?php echo @$data->name;?></h6> 
                                                </div> 
                                            </div>  
                                            <?php } ?>
                                       </div>
                                    </div>
                                    <!-- End Actors  -->
                                    <!-- Trailer -->
                                    <div class="tab-pane fade" role="tabpanel" id="trailer" aria-labelledby="trailer-tab"> 
                                        <div class="row" style="padding:10px;">
                                            <div class="modal-body">
                                            <iframe title="YouTube video player" class="youtube-player" type="text/html" width="100%" height="420" src="http://www.youtube.com/embed/<?php echo $video_id; ?>" frameborder="0" allowFullScreen></iframe> 
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End Trailer -->
                                </div> 
                            </div>
                        </div>
                        <!-- end tab additional content -->

                    </div>
                </div>
                <div class="col-md-3">  
                    <div class="row">
                        <div class="col-md-12">
                            <div class="heading-now">Now PLaying</div>
                        </div>
                        <!-- now playing -->
                        <div class="col-md-12">
                            <div class="now-playing-margin">
                            <a href="<?php echo get_permalink(); ?>">
                                <div class="row">
                                    <div class="col-md-3"> 
                                        <img src="<?php echo get_post_meta(get_the_ID(),'wpviddycpa-meta-image',TRUE) ?>" width="50">
                                    </div>
                                    <div class="col-md-9" style="text-align:left">
                                        <div class="now-title"><?php echo get_the_title(); ?></div>
                                        <div class="now-year">8989</div>
                                        <div>
                                             <span class="label-no-radius label-viddy-black" rel="tooltip" title="IMDB Rate">
                                            <i class="fa fa-star-o yellow"></i> <?php echo get_post_meta(get_the_ID(),'wpviddycpa-meta-rating',TRUE) ?>
                                        </span>  
                                        </div>
                                    </div>
                                </div>
                            </a>
                            </div>
                        </div>
                        <!-- end now playing --> 
                        <!-- now playing -->
                        <div class="col-md-12">
                            <div class="now-playing-margin">
                            <a href="<?php echo get_permalink(); ?>">
                                <div class="row">
                                    <div class="col-md-3"> 
                                        <img src="<?php echo get_post_meta(get_the_ID(),'wpviddycpa-meta-image',TRUE) ?>" width="50">
                                    </div>
                                    <div class="col-md-9" style="text-align:left">
                                        <div class="now-title"><?php echo get_the_title(); ?></div>
                                        <div class="now-year">8989</div>
                                        <div>
                                             <span class="label-no-radius label-viddy-black" rel="tooltip" title="IMDB Rate">
                                            <i class="fa fa-star-o yellow"></i> <?php echo get_post_meta(get_the_ID(),'wpviddycpa-meta-rating',TRUE) ?>
                                        </span>  
                                        </div>
                                    </div>
                                </div>
                            </a>
                            </div>
                        </div>
                        <!-- end now playing -->
                    </div>
                </div>

                <!-- Modal -->
                        <div class="modal fade" id="pop-download" tabindex="-1" role="dialog" aria-labelledby="pop-downloadLabel">
                          <div class="modal-dialog register-full-movie-lg" role="document">
                            <div class="modal-content register-full-movie"> 
                             
                             <div class="top-content" style="background-image:url(<?php echo $url_image; ?>)">
                                <p class="text-center top"><?php echo $title; ?></p><p class="text-center bottom">Released Date: <?php echo get_post_meta(get_the_ID(),'wpviddycpa-meta-release',TRUE); ?></p>
                             </div>
                             <div class="bottom-content"><img class="img-responsive" src="http://www.movioz.co/wp-content/themes/magelo/img/offer.png" width="614" height="275">
                             <p class="text-center"><button class="btn btn-offer btn-primary btn-lg reg" data-title="<?php echo $title; ?>" id="<?php echo @$filter_url_cpa[0]; ?>">Register Free Account</button></p>
                             </div>

                            </div>
                          </div>
                        </div>

                        <!-- Modal -->
                        <div class="modal fade" id="server-download" tabindex="-1" role="dialog" aria-labelledby="pop-downloadLabel">
                          <div class="modal-dialog register-full-movie-lg" role="document">
                            <div class="modal-content register-full-movie"> 
                             
                             <div class="top-content" style="background-image:url(<?php echo $url_image; ?>)">
                                <p class="text-center top"><?php echo $title; ?></p><p class="text-center bottom">Released Date: <?php echo get_post_meta(get_the_ID(),'wpviddycpa-meta-release',TRUE); ?></p>
                             </div>
                             <div class="bottom-content"><img class="img-responsive" src="http://www.movioz.co/wp-content/themes/magelo/img/offer.png" width="614" height="275">
                             <p class="text-center"><button class="btn btn-offer btn-primary btn-lg reg" data-title="<?php echo $title; ?>" id="<?php echo @$filter_url_cpa[0]; ?>">Register Free Account</button></p>
                             </div>

                            </div>
                          </div>
                        </div>

            </div>


            <!-- comment -->
            <div class="row"> 
                <div class="col-md-9 fb-comment-"> 
                    <div class="fb-comments" data-href="<?php echo $get_permalink; ?>" data-colorscheme="dark" data-width="100%" data-numposts="5"></div>
                </div>
            </div>
            <!-- end comment -->

        </div>
        <!-- /.container -->

    </div>
    <!-- /.intro-header -->


     
    <div class="content-section-a">

        <div class="container">
            <div class="row no-getter"> 
                <div class="col-md-12"><h3 class="heading">Related Movie</h3></div>

                <!-- Related Movie -->
                <?php  
                   //$currentCategory = single_cat_title("", false);
                    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                    $args = array(
                        'post_type' => 'post',
                        //'gallery_category' => 'video-gallery',
                        'paged'=> $paged,
                        'order' => 'DESC',
                        'posts_per_page' => '12'
                    );  
                    $the_query = new WP_Query( $args ); 
                if($the_query->have_posts()) :     
                    while ($the_query->have_posts()):
                        $the_query->the_post();   
                        $featured_image_array = wp_get_attachment_image_src( get_post_thumbnail_id(), 'single-post-thumbnail' );
                        $featured_image = $featured_image_array[0];
                   ?>  
                    <div class="col-md-2 col-xs-4 box-thumb-images"> 
                        <div class="thumbnail no-border">
                            <a href="<?php echo get_permalink(); ?>"> 
                            <img class="img-responsive" src="<?php echo get_post_meta(get_the_ID(),'wpviddycpa-meta-image',TRUE) ?>">
                            <div class="caption-play">
                                <img src="http://mtdb.info/assets/images/play.png">
                            </div>
                            <div class="caption">  
                                <span class="label-no-radius label-viddy-black" rel="tooltip" title="IMDB Rate">
                                    <i class="fa fa-star-o yellow"></i> <?php echo get_post_meta(get_the_ID(),'wpviddycpa-meta-rating',TRUE) ?>
                                </span>  
                            </div>
                            </a>
                        </div>
                        <div class="box-title-thumb">
                            <a href="<?php echo get_permalink(); ?>" class="text-left">
                                <h5><?php echo get_the_title(); ?></h5> 
                            </a>
                        </div>
                    </div>
                    <?php endwhile; ?>
                <?php endif; ?>
                <!-- End Related Movie --> 

        </div>
        <!-- /.container -->

    </div>
    <!-- /.content-section-a -->
  
    

<?php } ?>
 

<?php 
if(is_search()){
?>
 
<?php } ?>
 