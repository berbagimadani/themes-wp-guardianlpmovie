  
 
    <!-- 
    <div class="banner-footer">
        <div class="container">
            <div class="row no-getter pull-right"> 
                <div class="col-md-12">
                    <div id="share"></div>
                </div>
            </div>
        </div> 
    </div> 
    -->
    
    <!-- Footer -->
    <footer class="bg-footer">
        <div class="container">
            <div class="row no-getter ">
                <div class="col-md-5">
                    <p class="copyright text-muted small"> 
                    <?php echo getOption('copyright'); ?>
                    </p>
                </div>
                <div class="col-lg-7" style="text-align:right">  
                    <?php if ( has_nav_menu( 'primary' ) ) : ?> 
                    <?php
                        // Primary navigation menu.
                        wp_nav_menu( array(
                            'menu_class'     => 'list-inline',
                            'theme_location' => 'primary',
                        ) );
                    ?> 
                    <?php endif; ?>  
                </div>

            </div>
        </div>
    </footer>

    <style type="text/css">
        .online {
            color: #fff;
            font-size: 12px;
            position: fixed;
            bottom: 0;
            right: 0;
            background: rgba(0,0,0,0.5);
            width: 175px;
            padding: 10px;
            border-radius: 5px 0 0 0;
            z-index: 999999
        }
        .online-icon {
            display: inline-block;
            background: url(http://forums.getpaint.net/public/style_images/master/ajax_loading.gif) no-repeat;
            width: 43px;
            height: 11px;
        }
        .online-text {
            width: auto;
            float: right;
            display: inline-block; 
        }
         .online-text p{
            font-size: 12px;
        }
        </style>
    <!--
    <div class="online">
        <span class="online-icon"></span>
        <div class="online-text">
            <p>
            <?php
            
            $rand = rand(999,true); 
            //echo 'The random number is: '.$rand.'<br><br>'; 

            if( isset($_SESSION['rand']) ) { 
               $_SESSION['rand'] += $rand; 
            } else { 
               $_SESSION['rand'] = $rand; 
            } 

            echo $_SESSION['rand'];

            ?> Users Online Now</p>
        </div>
    </div>-->


    <!-- popup -->
    <script src="<?php echo  get_template_directory_uri(). '/js/jquery.popup.min.js'; ?>"></script> 
    <!-- player -->
    <script src="<?php echo  get_template_directory_uri(). '/js/plyr.js'; ?>"></script>
    <!-- custome --> 

    <script src="<?php echo  get_template_directory_uri(). '/social-share/jssocials.min.js'; ?>"></script>

    <script src="<?php echo  get_template_directory_uri(). '/js/jquery.cookie.js'; ?>"></script>

    <script src="https://www.youtube.com/iframe_api"></script>

    

    <script type="text/javascript"> 
    
    /*
    * HOVER
    */
    $( document ).ready(function() {

        jQuery( ".reg" ).on('click',function() { 
            var id = jQuery( this ).text() + " = " + jQuery( this ).attr( "id" );
            alert( "Handler for .click() called." + id);
        });

            $("[rel='tooltip']").tooltip();    
         
            $('.thumbnail').hover(
                function(){
                    $(this).find('.caption').slideDown(250); //.fadeIn(250)
                },
                function(){
                    $(this).find('.caption').slideUp(250); //.fadeOut(205)
                }
            ); 
    });
    </script>
     <script>
        $("#share").jsSocials({
            shares: ["twitter", "facebook", "googleplus", "pinterest"]
        });
    </script> 

<!-- 
    New PLayer Youtube
-->
<script type="text/javascript">
   
    window.onload = function() { 
          // Buttons
          var playButton = document.getElementById("play_btn"); 
          var playButtonCover = document.getElementById("play_btn_cover"); 
          var playServer = document.getElementById("play-server");
          var playServer2 = document.getElementById("play-server2");

          var top_top = document.getElementById("top-top");
          var top_bottom = document.getElementById("top-bottom");
         
        playButton.addEventListener("click", function() {            
            top_top.className += " active";
            top_bottom.className += " active";
            $(".top-background").remove(); 
            playYtb();
            //("#visi").hide();   
        });
        playButtonCover.addEventListener("click", function() {            
            top_top.className += " active";
            top_bottom.className += " active";
            $(".top-background").remove(); 
            playYtb();
            //("#visi").hide();      
        });
        playServer.addEventListener("click", function() {            
            top_top.className += " active";
            top_bottom.className += " active";
            $(".top-background").remove(); 
            playYtb();

            jQuery('body,html').animate({
                scrollTop: 0 ,
                 }, 400
             );
        });
        playServer2.addEventListener("click", function() {            
            top_top.className += " active";
            top_bottom.className += " active";
            $(".top-background").remove(); 
            playYtb();

            jQuery('body,html').animate({
                scrollTop: 0 ,
                 }, 400
            );
        });
    }

    var player, iframe;
    var $ = document.querySelector.bind(document);

    var fullytb = document.getElementById("fullscreen_btn");

    // init player
    function onYouTubeIframeAPIReady() {
      player = new YT.Player('player', {
        height: '100%',
        width: '100%',
        videoId: '<?php echo introBackground(); ?>',
        playerVars: { 'modestbranding': 1, 'controls': 0, 'rel': 0, 'hd':1, 'showinfo':0 },
        events: {
          'onReady': onPlayerReady,
          'onStateChange': onPlayerStateChange
        }
      });
    }

    // when ready, wait for clicks
    function onPlayerReady(event) {
      var player = event.target;
      iframe = $('#element');
      setupListener(); 
    }
    
    var done = false;
    function onPlayerStateChange(event) {
        if (event.data == YT.PlayerState.PLAYING && !done) {
            setTimeout(stopVideo, 3000);
            
            done = true;
        }
    }
    function stopVideo() {
        player.stopVideo();
        //jQuery('#pop-download').modal('show');
        jQuery('#pop-download').modal({
          keyboard: false,
          backdrop: 'static',
          show: true
        })
    }

    function setupListener (){  
        var e = document.getElementById("fullscreen_btn");
        e.onclick = function() {
            if (RunPrefixMethod(document, "FullScreen") || RunPrefixMethod(document, "IsFullScreen")) {
                RunPrefixMethod(document, "CancelFullScreen");
            }
            else {
                RunPrefixMethod(iframe, "RequestFullScreen");
            }
        }
    }

    function playYtb(){
        player.playVideo();
    }

      
    var pfx = ["webkit", "moz", "ms", "o", ""];
    function RunPrefixMethod(obj, method) {
        
        var p = 0, m, t;
        while (p < pfx.length && !obj[m]) {
            m = method;
            if (pfx[p] == "") {
                m = m.substr(0,1).toLowerCase() + m.substr(1);
            }
            m = pfx[p] + m;
            t = typeof obj[m];
            if (t != "undefined") {
                pfx = [pfx[p]];
                return (t == "function" ? obj[m]() : obj[m]);
            }
            p++;
        }

    }
</script>
    

<?php wp_footer(); ?>
  </body>
</html>
